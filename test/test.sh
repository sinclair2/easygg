#!/bin/bash

TESTS_ALL=0
TESTS_FAIL=0


# $1 = test name
# $2 = command
# $3 = expected status code
runtest()
{
	TESTS_ALL=$((TESTS_ALL + 1))
	echo "####################"
	echo "Running test ${TESTS_ALL}: $1"
	OUTPUT="$( eval "$2" )"
	export RET=$?
	export OUTPUT
	if eval "$3";
	then
		echo "OK"
	else
		echo -e "${OUTPUT}"
		echo "FAILED"
		TESTS_FAIL=$((TESTS_FAIL + 1))
	fi
}

# tests if it stops when no images are found
runtest "noimages" "../easygg 2>&1" "[ \$RET -eq 1 ] && ( echo \"\$OUTPUT\" | grep \"No image files found.\" >/dev/null )"

cd dataset1 || exit 1

# tests if a normal run is successful
runtest "dataset1 execution" "../../easygg 2>&1" "[ \$RET -eq 0 ] && ( echo \"\$OUTPUT\" | grep \"Finished.\" >/dev/null )"

# tests if expected number of files are produced
runtest "dataset1 files" "echo gallery/* | md5sum | grep \"53a7b7811da010336ef3f1197f8d39ee\"" "[ \$RET -eq 0 ]"

# tests if all *.html files produce no parsing errors
runtest "dataset1 html validation" "xmllint --noout --valid --html gallery/*.html 2>&1" "[ \$RET -eq 0 ] && [ -z \"\$OUTPUT\" ]"

# tests if all page links are generated
runtest "dataset1 content" "grep -e '<a href=\"page_.*\\.jpg.html\"><img src=\"thumb_.*\\.jpg\"' gallery/index.html | wc -l" "[ \$RET -eq 0 ] && [ \"\$OUTPUT\" = \"16\" ]"

rm -rf gallery


echo "####################"

if [ ${TESTS_FAIL} -eq 0 ];
then
	echo "All ${TESTS_ALL} tests successful."
	exit 0
else
	echo "${TESTS_FAIL} of ${TESTS_ALL} tests failed."
	exit 1
fi

