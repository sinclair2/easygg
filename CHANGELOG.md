# ChangeLog

## [0.6.0] 2018-07-12
### Added
- new -H/--header option for HTML code to be inserted on top of each page

### Changed
- removed "Generated by" footer
- changed default name for subdirectory from "easygg" to "gallery", when the
  script was in the current directory it was blocking the creation of the
  subdirectory


## [0.5.1] 2018-07-10
### Changed
- HTML pages generated with charset utf-8
- check for image files before generating directoy structure, so if none are
  found, nothing will be written
- do not print version number and dates in html files so that an execution on
  the same data always produces the same output which makes it easier to test
- fixed issues found by shellcheck
- removed version from script and --version option


## [0.5] 2012-09-04
### Added
- added LICENSE.txt
- added README.md

### Changed
- renamed options -sd/--slide-dimension -fd/--full-dimension
- use http://keepachangelog.com formatting for this CHANGELOG


## [0.4] 2010-12-11
### Added
- width and height of scaled images is used in html image tag
- handle PNG files now too, auto-rotation won't work there though
- conforming XHTML 1.0 strict now, instead of HTML 4.01 transitional
- command line options for overriding default title and subdir names, plus
  the usual verbose, quiet, help, version options. all command line options
  are entirely optional, so the easy "run and be done" way still works.
- thumbnail and slide dimensions can be adjusted (also via commandline option)

### Changed
- added/moved some layout to CSS
- black image border now added via CSS instead directly to the image by
  convert (speeds up thumbnail generation)
- many bugfixes
- license change from GPLv2 to GPLv3

## [0.3] 2004-02-14
### Added
- Spaces in filenames will be processed now. Spaces are not allowed in
  URLs (according to the RFC), thus spaces will be converted to
  underscores in the generated pages.
- There is more verbose output and the verbosity can be adjusted.
- This release will check to see if all tools are there before it starts.

### Changed
- Images will be sorted alphabetically.

