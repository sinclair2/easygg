easyGG
======

easyGG is an automatic gallery generator.


Features
--------

- automatic generation of linked HTML pages with index
- automatic image rotation based on EXIF info
- automatic scaling for web usage


Usage
-----

Just call the script from a directory with JPG or PNG images and it will create a subdirectory with a ready to use gallery. You can override several default options though. Call the script with parameter *--help* for help.

See the [-> [**demo**](https://su2.info/easygg/demo/) <-] how an easyGG generated gallery looks like.
That are pictures from a trip to the nice city Strasbourg :)


Prerequisites
-------------
The program is a Bash shell script. It uses the tools `convert` from [ImageMagick](http://www.imagemagick.org/) and [`jhead`](http://www.sentex.net/~mwandel/jhead/). If you don't have them, install them first.


License
-------
The software is [free software](https://www.fsf.org/about/what-is-free-software) and licensed under the [GNU GPL](https://www.gnu.org/licenses/gpl.html).


Contact
-------
If you experience problems with the script or have any suggestions just [contact me](mailto:su@su2.info).

Have fun,
Stephan

